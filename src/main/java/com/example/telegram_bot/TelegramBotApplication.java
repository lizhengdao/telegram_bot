package com.example.telegram_bot;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Log
@SpringBootApplication
public class TelegramBotApplication {

    private static RestTemplate restTemplate;

    @Autowired
    public TelegramBotApplication(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public static void main(String[] args) {
        SpringApplication.run(TelegramBotApplication.class, args);
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new MyAmazingBot(restTemplate));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
        System.out.println("Hello! I am Telegram Bot. Let's start!");
    }
}


// DmitryProBot
// username for your bot: DmitryPro2_bot

//Use this token to access the HTTP API:
//        734820024:AAEPZch83qM1Z0QczQrphdHHZpa8tELfM5Q