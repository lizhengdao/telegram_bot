package com.example.telegram_bot;

import com.example.telegram_bot.model.TelegrammCity;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Log
public class MyAmazingBot extends TelegramLongPollingBot {

    private RestTemplate restTemplate;

    public MyAmazingBot(RestTemplate restTemplate) {
        System.out.println("test - RestTemplate is initialized!");
        this.restTemplate = restTemplate;
    }

    @Override
    public void onUpdateReceived(Update update) {

        String stringFromBot = update.getMessage().getText();
        String userFirstName = update.getMessage().getFrom().getFirstName();
        SendMessage sendMessage = new SendMessage();
        String url_local = "http://localhost:8090/telegram/city/" + stringFromBot; // local
        String url_server = "http://q11.jvmhost.net/telegram/city/" + stringFromBot; // with server vmcncnc

        switch (stringFromBot) {
            case "Hello", "Hello!", "hello!", "hello", "Hi":
                sendMessage.setText("Hello, " + userFirstName + "! Nice to meet you! Please enter city!");
                break;

            case "Hello, how are you?":
                sendMessage.setText("Hello, " + userFirstName + "! I am fine thanks! How are you?");
                break;

            case "How are you?", "how are you?", "how are you":
                sendMessage.setText("I am fine thanks! How are you? Please enter city!");
                break;

            default:
                ResponseEntity<TelegrammCity> response = restTemplate.getForEntity(url_server, TelegrammCity.class); // dm
                if (response.getBody() != null) {
                    System.out.println("-------------- " + response.getBody());
                    sendMessage.setText(response.getBody().getDescription());
                } else {
                    sendMessage.setText("Please enter city!(Avaluble: Minsk, Moscow, Kiev)");
                }
                break;
        }

        sendMessage.setChatId(update.getMessage().getChatId());

        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getBotUsername() {
        return "DmitryPro2_bot";
    }

    @Override
    public String getBotToken() {
        return "734820024:AAEPZch83qM1Z0QczQrphdHHZpa8tELfM5Q";
    }
}

// DmitryProBot
// username for your bot: DmitryPro2_bot
// Use this token to access the HTTP API: 734820024:AAEPZch83qM1Z0QczQrphdHHZpa8tELfM5Q